import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';

import {
  ProductContainer,
  Quantity,
  IncreaseQuantityButton,
  DecreaseQuantityButton,
} from './CartProduct.styles';

const CartProduct = (props) => {
  const {
    name,
    price,
    quantity,
    onQuantityDecrease,
    onQuantityIncrease,
  } = props;

  return (
    <ProductContainer>
      <Flex justify="space-between">
        <Box width={3 / 5}>{name}</Box>
        <Box width={1 / 5}>${price}</Box>

        <Box width={1 / 5}>
          <Flex>
            <IncreaseQuantityButton onClick={onQuantityDecrease}>-</IncreaseQuantityButton>
            <Quantity>{quantity}</Quantity>
            <DecreaseQuantityButton onClick={onQuantityIncrease}>+</DecreaseQuantityButton>
          </Flex>
        </Box>
      </Flex>
    </ProductContainer>
  );
};

CartProduct.defaultProps = {
  name: '',
  price: null,
  quantity: null,
  onQuantityDecrease: () => {},
  onQuantityIncrease: () => {},
};

CartProduct.propTypes = {
  name: PropTypes.string,
  price: PropTypes.number,
  quantity: PropTypes.number,
  onQuantityDecrease: PropTypes.func,
  onQuantityIncrease: PropTypes.func,
};

export default CartProduct;
