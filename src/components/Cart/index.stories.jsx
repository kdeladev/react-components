import React from 'react';
import { Provider, connect } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text } from '@storybook/addon-knobs/react';

import styled from 'styled-components';

import withTests from 'storybook-addon-jest';
import jestTestResults from '../../../.jest-test-results.json';

import products from './__mocks__/products';

import { Cart, cartReducer, cartActions, cartSelectors } from './index';
import { CheckoutButton } from './Cart.styles';

const StyledCart = styled(Cart)`
  font-family: Helvetica, Arial, Sans-Serif;
  width: 300px;
  height: 300px;
  border: 1px solid #d4d4d4;
  border-radius: 3px;
  box-shadow: 1px 1px 20px 0px #dedede;
  margin-left: 30px;
  margin-top: 30px;
`;

const StyledCheckoutButton = styled(CheckoutButton)`
  background-color: #7b7b7b;
  color: #fff;
`;

const CartActionsWrapper = styled.div`
  margin-top: 25px;

  & > * {
    display: block;
    margin-bottom: 10px;
  }
`;

const CartActionsButton = styled(CheckoutButton)``;

const getDefaultProps = ext => Object.assign({}, ext, {
  title: text('Title', 'Cart'),
  onCheckoutAction: action('onCheckoutAction'),
});

storiesOf('Cart', module)
  .addDecorator(withKnobs)
  .addDecorator(withTests(jestTestResults, { filesExt: '.test.js' })('Cart', 'CartProduct', 'CartRedux', 'CartReduxActions', 'CartReduxSelectors'))
  .add('with redux', () => {
    const store = createStore(
      combineReducers({
        cart: cartReducer,
      }),
      applyMiddleware(thunk),
    );

    const mapCartStateToProps = state => ({
      title: text('Title', 'Cart'),
      products: cartSelectors.getProducts(state.cart),
      onCheckoutAction: action('onCheckoutAction'),
    });

    const mapCartDispatchToProps = dispatch => ({
      onQuantityChange: (product, quantity) => {
        if (quantity <= 0) {
          return dispatch(cartActions.removeProduct(product));
        }

        return dispatch(cartActions.updateQuantity(product, quantity));
      },
    });

    const ConnectedCart = connect(mapCartStateToProps, mapCartDispatchToProps)(StyledCart);

    // eslint-disable-next-line react/prop-types
    const CartActionsContainer = ({ addProduct }) => (
      <CartActionsWrapper>
        <CartActionsButton onClick={() => addProduct(products[0])}>Add product 1</CartActionsButton>
        <CartActionsButton onClick={() => addProduct(products[1])}>Add product 2</CartActionsButton>
        <CartActionsButton onClick={() => addProduct(products[2])}>Add product 3</CartActionsButton>
      </CartActionsWrapper>
    );

    const mapDispatchToProps = dispatch => ({
      addProduct: product => dispatch(cartActions.addProductToCart(product)),
    });

    const ConnectedCartActions = connect(null, mapDispatchToProps)(CartActionsContainer);

    return (
      <Provider store={store}>
        <div>
          <ConnectedCart />
          <ConnectedCartActions />
        </div>
      </Provider>
    );
  })
  .add('custom checkout', () => {
    const props = getDefaultProps({
      products: products.map(product => Object.assign({}, product, {
        quantity: 1,
      })),
      checkoutButton: <StyledCheckoutButton>Custom checkout</StyledCheckoutButton>,
    });

    return (
      <StyledCart {...props} />
    );
  });
