/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { render } from 'react-dom';
import App from './App';

const root = document.getElementById('root');

render(
  (
    <App />
  ), root,
);
