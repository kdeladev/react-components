import React, { Component } from 'react';
import PropTypes from 'prop-types';

import CartProduct from './CartProduct';

import {
  CartContainer,
  Header,

  ProductsList,

  Footer,
  CheckoutButton,
} from './Cart.styles';

class Cart extends Component {
  static propTypes = {
    title: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.bool,
    ]),
    products: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      price: PropTypes.number,
      quantity: PropTypes.number,
      onQuantityIncrease: PropTypes.func,
      onQuantityDecrease: PropTypes.func,
    })),
    checkoutButton: PropTypes.element,
    onCheckoutAction: PropTypes.func,
    onQuantityChange: PropTypes.func,
    className: PropTypes.string,
  }

  static defaultProps = {
    title: 'Cart',
    products: [],
    checkoutButton: <CheckoutButton>Checkout</CheckoutButton>,
    onCheckoutAction: () => {},
    onQuantityChange: () => {},
    className: '',
  }

  pickProductProps = (product) => {
    const {
      id, name, price, quantity,
    } = product;

    return {
      id, name, price, quantity,
    };
  }

  render() {
    return (
      <CartContainer className={this.props.className}>
        {this.props.title !== false &&
          <Header>{this.props.title}</Header>
        }
        <ProductsList>
          {this.props.products.map(product => (
            <CartProduct
              key={product.id}
              onQuantityDecrease={() => this.props.onQuantityChange(product, product.quantity - 1)}
              onQuantityIncrease={() => this.props.onQuantityChange(product, product.quantity + 1)}
              {...this.pickProductProps(product)}
            />
          ))}
        </ProductsList>
        <Footer>
          {
            React.cloneElement(this.props.checkoutButton, {
              onClick: e => this.props.onCheckoutAction(e),
            })
          }
        </Footer>
      </CartContainer>
    );
  }
}

export default Cart;
