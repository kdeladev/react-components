import { expect } from 'chai';
import { mount } from 'enzyme';
import React from 'react';

import products from './__mocks__/products';

import Cart from './Cart';
import CartProduct from './CartProduct';

import {
  Header,
  CheckoutButton,
} from './Cart.styles';

describe('Cart should render', () => {
  it('with empty products list', () => {
    const wrapper = mount(<Cart />);

    expect(wrapper.find(CartProduct)).to.have.length(0);
  });
  it('with filled products list', () => {
    const wrapper = mount(<Cart products={products} />);

    expect(wrapper.find(CartProduct)).to.have.length(products.length);
  });
});

describe('Cart header should', () => {
  it('should render default title', () => {
    const wrapper = mount(<Cart />);

    expect(wrapper.find(Header).text())
      .to.be.an('string').and
      .to.be.equal(Cart.defaultProps.title);
  });

  it('render with passed title', () => {
    const title = 'Passed title';
    const wrapper = mount(<Cart title={title} />);

    expect(wrapper.find(Header).text())
      .to.be.an('string').and
      .to.be.equal(title);
  });

  it('render with passed empty title', () => {
    const wrapper = mount(<Cart title="" />);

    expect(wrapper.find(Header).text())
      .to.be.an('string').and
      .to.be.equal('');
  });

  it('not render with empty title', () => {
    const wrapper = mount(<Cart title={false} />);

    expect(wrapper.find(Header)).to.have.length(0);
  });
});

describe('Cart checkout button should', () => {
  it('be rendered', () => {
    const wrapper = mount(<Cart />);

    expect(wrapper.find(CheckoutButton)).to.have.length(1);
  });

  it('fire action on default checkout click', () => {
    const onCheckoutMockFn = jest.fn();

    const wrapper = mount(<Cart onCheckoutAction={onCheckoutMockFn} />);

    wrapper.find(CheckoutButton).simulate('click');

    expect(onCheckoutMockFn.mock.calls).to.have.length(1);
  });
});

describe('Cart custom checkout button should', () => {
  const btnClassName = 'test-checkout-button';
  let btn;

  beforeEach(() => {
    btn = React.createElement('button', {
      className: btnClassName,
    });
  });

  it('be rendered', () => {
    const wrapper = mount(<Cart checkoutButton={btn} />);

    expect(wrapper.find(`.${btnClassName}`).exists()).to.equal(true);
  });

  it('fire action on default checkout click', () => {
    const onCheckoutMockFn = jest.fn();

    const wrapper = mount(<Cart checkoutButton={btn} onCheckoutAction={onCheckoutMockFn} />);

    wrapper.find(`.${btnClassName}`).simulate('click');

    expect(onCheckoutMockFn.mock.calls).to.have.length(1);
  });
});
