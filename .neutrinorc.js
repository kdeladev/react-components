const { merge } = require('@neutrinojs/compile-loader');

module.exports = {
  use: [
    '@neutrinojs/react-components',
    ['@neutrinojs/jest', {
      setupTestFrameworkScriptFile: '<rootDir>/src/setupTests.js',
      setupFiles: [
        '<rootDir>/test/shim.js'
      ]
    }],
    ['@neutrinojs/airbnb', {
      eslint: {
        globals: ['document', 'it', 'beforeEach', 'describe', 'jest'],
        rules: {
          'babel/new-cap': 'off',
          'no-case-declarations': 'off'
        }
      }
    }]
  ]
};

