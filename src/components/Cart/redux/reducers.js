import update from 'immutability-helper';
import get from 'lodash/get';

import {
  ADD_PRODUCT,
  REMOVE_PRODUCT,
  UPDATE_QUANTITY,
} from './types';

export const initialState = {
  productsByIds: {},
  quantityByIds: {},
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_PRODUCT:
      const product = get(action, 'payload.product');

      if (!product) {
        return state;
      }

      return update(state, {
        productsByIds: {
          $merge: {
            [product.id]: product,
          },
        },
        quantityByIds: {
          $merge: {
            [product.id]: action.payload.quantity || 1,
          },
        },
      });
    case REMOVE_PRODUCT:
      return update(state, {
        productsByIds: {
          $unset: [action.payload.id],
        },
        quantityByIds: {
          $unset: [action.payload.id],
        },
      });
    case UPDATE_QUANTITY:
      return update(state, {
        quantityByIds: {
          $merge: {
            [action.payload.id]: action.payload.quantity,
          },
        },
      });
    default:
      return state;
  }
};

export default cartReducer;
