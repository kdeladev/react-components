import styled from 'styled-components';
import { Box } from 'grid-styled';

const ProductContainer = styled.div`
  border-bottom: 1px solid #d4d4d4;
  padding: 10px;
`;

const QuantityButton = styled(Box)`
  background-color: #cacaca;
  color: white;
  cursor: pointer;
  user-select: none;
  text-align: center;
  width: 19px;
  min-width: 19px;
  height: 19px;
  border-radius: 50%;
`;

const IncreaseQuantityButton = styled(QuantityButton)``;
const DecreaseQuantityButton = styled(QuantityButton)``;

const Quantity = styled(Box)`
  padding-left: 5px;
  padding-right: 5px;
`;

export {
  ProductContainer,
  Quantity,
  IncreaseQuantityButton,
  DecreaseQuantityButton,
};
