import { expect } from 'chai';
import { mount } from 'enzyme';
import React from 'react';
import pick from 'lodash/pick';

import products from './__mocks__/products';

import CartProduct from './CartProduct';

import {
  IncreaseQuantityButton,
  DecreaseQuantityButton,
} from './CartProduct.styles';

describe('Product should', () => {
  it('trigger quantity change actions', () => {
    const onIncreaseMockFn = jest.fn();
    const onDecreaseMockFn = jest.fn();

    const wrapper = mount(<CartProduct
      {...pick(products[0], ['id', 'name', 'quantity'])}
      onQuantityDecrease={onIncreaseMockFn}
      onQuantityIncrease={onDecreaseMockFn}
    />);

    wrapper.find(IncreaseQuantityButton).simulate('click');
    wrapper.find(DecreaseQuantityButton).simulate('click');

    expect(onIncreaseMockFn.mock.calls).to.have.length(1);
    expect(onDecreaseMockFn.mock.calls).to.have.length(1);
  });
});
