Otworzenie komponentu bez odpalania środowiska:

  - Otworzyć w przeglądarce plik /storybook-static/index.html

Otworzenie komponentu z odpaleniem środowiska:

  - yarn
  - yarn test:output (dla wygenerowania pliku statycznego z testami zaciaganymi przez storybook)
  - yarn start
