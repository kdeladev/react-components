import { expect } from 'chai';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nth from 'lodash/nth';

import { initialState } from './reducers';

import mockProducts from '../__mocks__/products';

import {
  addProduct,
  removeProduct,
  updateQuantity,
  addProductToCart,
} from './actions';

import {
  ADD_PRODUCT,
  REMOVE_PRODUCT,
  UPDATE_QUANTITY,
} from './types';

const mockStore = configureStore([thunk]);

describe('Should', () => {
  let store;
  let product;

  beforeEach(() => {
    store = mockStore(initialState);
    product = nth(mockProducts, 0);
  });

  it('dispatch ADD_PRODUCT action', () => {
    store.dispatch(addProduct(product));
    store.dispatch(addProduct(product, 1));
    store.dispatch(addProduct(product, 3));

    const actions = store.getActions();

    expect(actions).to.deep.equal([{
      type: ADD_PRODUCT,
      payload: {
        product,
        quantity: 1,
      },
    }, {
      type: ADD_PRODUCT,
      payload: {
        product,
        quantity: 1,
      },
    }, {
      type: ADD_PRODUCT,
      payload: {
        product,
        quantity: 3,
      },
    }]);

    expect(actions).to.be.length(3);
  });


  it('dispatch REMOVE_PRODUCT action', () => {
    store.dispatch(removeProduct({
      id: product.id,
    }));

    const actions = store.getActions();

    expect(actions).to.deep.equal([{
      type: REMOVE_PRODUCT,
      payload: {
        id: product.id,
      },
    }]);

    expect(actions).to.be.length(1);
  });

  it('dispatch UPDATE_QUANTITY action', () => {
    store.dispatch(updateQuantity(product, 3));

    const actions = store.getActions();

    expect(actions).to.deep.equal([{
      type: UPDATE_QUANTITY,
      payload: {
        id: product.id,
        quantity: 3,
      },
    }]);

    expect(actions).to.be.length(1);
  });

  it('dispatch ADD_PRODUCT if product is in NOT the store', () => {
    const storeKey = 'cart';

    const localStore = mockStore({
      [storeKey]: {
        productsByIds: {},
        quantityByIds: {},
      },
    });

    localStore.dispatch(addProductToCart(product, 1, storeKey));

    const actions = localStore.getActions();

    expect(actions[0]).to.deep.equal({
      type: ADD_PRODUCT,
      payload: {
        product,
        quantity: 1,
      },
    });
  });

  it('dispatch UPDATE_QUANTITY if product is in the store', () => {
    const storeKey = 'cart';

    const localStore = mockStore({
      [storeKey]: {
        productsByIds: {
          [product.id]: product,
        },
        quantityByIds: {
          [product.id]: 3,
        },
      },
    });

    localStore.dispatch(addProductToCart(product, 1, storeKey));

    const actions = localStore.getActions();

    expect(actions[0]).to.deep.equal({
      type: UPDATE_QUANTITY,
      payload: {
        id: product.id,
        quantity: 4,
      },
    });
  });
});
