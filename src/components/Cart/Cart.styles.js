import styled from 'styled-components';
import { Flex } from 'grid-styled';

const CartContainer = styled.div`
  flex-direction: column;
  display: flex;
  background-color: #fff;
  overflow: hidden;
`;

const Header = styled.div`
  flex: 0 1 auto;
  font-size: 1.3rem;
  padding: 10px 0px 10px 10px;
  border-bottom: 1px solid #d4d4d4;
  box-shadow: 0px 3px 11px 0px #dedede;
  min-height: 25px;
`;

const ProductsList = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  overflow-y: auto;
`;

const Footer = styled(Flex)`
  flex: 0 1 auto;
  font-size: 1.3rem;
  padding: 10px 0px 10px 10px;
  border-top: 1px solid #d4d4d4;
`;

const CheckoutButton = styled.button`
  line-height: 1.5;
  display: inline-block;
  font-weight: 400;
  text-align: center;
  -ms-touch-action: manipulation;
  touch-action: manipulation;
  cursor: pointer;
  background-image: none;
  border: 1px solid transparent;
  white-space: nowrap;
  padding: 0 15px;
  font-size: 14px;
  border-radius: 4px;
  height: 32px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -webkit-transition: all 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
  transition: all 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
  position: relative;
  color: rgba(0, 0, 0, 0.65);
  background-color: #fff;
  border-color: #d9d9d9;
  outline: 0;
`;

export {
  CartContainer,
  Header,
  ProductsList,
  Footer,
  CheckoutButton,
};
