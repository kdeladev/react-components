import { createSelector } from 'reselect';

import flow from 'lodash/fp/flow';
import map from 'lodash/fp/map';
import values from 'lodash/values';

const productsSelector = cart => cart.productsByIds;
const quantitySelector = cart => cart.quantityByIds;

const getProducts = createSelector(
  productsSelector, quantitySelector,
  (products, quantity) => flow(
    map(product => Object.assign({}, product, {
      quantity: quantity[product.id],
    })),
    values,
  )(products),
);

export { getProducts };
export default { getProducts };
