import get from 'lodash/get';

import {
  ADD_PRODUCT,
  REMOVE_PRODUCT,
  UPDATE_QUANTITY,
} from './types';


export const addProduct = (product, quantity = 1) => ({
  type: ADD_PRODUCT,
  payload: {
    product,
    quantity,
  },
});

export const updateQuantity = (product, quantity = 1) => ({
  type: UPDATE_QUANTITY,
  payload: {
    id: product.id,
    quantity,
  },
});

export const removeProduct = product => ({
  type: REMOVE_PRODUCT,
  payload: {
    id: product.id,
  },
});

export const addProductToCart = (product, quantity = 1, storeKey = 'cart') => (dispatch, getState) => {
  const currentQuantity = get(getState(), `${storeKey}.quantityByIds.${product.id}`);

  if (!currentQuantity) {
    return dispatch(addProduct(product, quantity));
  }

  let nextQuantity = currentQuantity;
  nextQuantity += 1;
  return dispatch(updateQuantity(product, nextQuantity));
};
