import { expect } from 'chai';
import { Reducer } from 'redux-testkit';
import merge from 'lodash/merge';
import nth from 'lodash/nth';

import cartReducer, { initialState } from './reducers';
import mockProducts from '../__mocks__/products';

import {
  ADD_PRODUCT,
  REMOVE_PRODUCT,
  UPDATE_QUANTITY,
} from './types';

describe('Cart reducer', () => {
  let id;
  let product;

  let addProductAction;
  let removeProductAction;
  let updateQuantityAction;

  let emptyState;
  let initialStateWithProduct;

  beforeEach(() => {
    product = nth(mockProducts, 0);

    ({ id } = product);

    addProductAction = {
      type: ADD_PRODUCT,
      payload: {
        product,
      },
    };

    removeProductAction = {
      type: REMOVE_PRODUCT,
      payload: {
        id,
      },
    };

    updateQuantityAction = {
      type: UPDATE_QUANTITY,
      payload: {
        id,
        quantity: 1,
      },
    };

    emptyState = {
      productsByIds: {},
      quantityByIds: {},
    };

    initialStateWithProduct = cartReducer(undefined, addProductAction);
  });

  it('should have initial state', () => {
    const reducer = cartReducer(undefined, {});

    expect(reducer).to.be.equal(initialState);
  });

  it('should return the same state with non existing action', () => {
    const emptyAction = {
      type: '',
    };

    Reducer(cartReducer)
      .withState(initialStateWithProduct)
      .expect(emptyAction)
      .toReturnState(initialStateWithProduct);
  });

  it('should handle ADD_PRODUCT action with default quantity', () => {
    Reducer(cartReducer).expect(addProductAction).toReturnState(initialStateWithProduct);
  });

  it('should handle ADD_PRODUCT action with passed quantity', () => {
    const action = merge({}, addProductAction, { payload: { quantity: 3 } });

    const stateWithUpdatedQuantity = merge({}, initialStateWithProduct, {
      quantityByIds: {
        [id]: 3,
      },
    });

    Reducer(cartReducer)
      .expect(action)
      .toReturnState(stateWithUpdatedQuantity);
  });

  it('should handle ADD_PRODUCT action with empty payload', () => {
    addProductAction.payload = {};

    Reducer(cartReducer)
      .expect(addProductAction)
      .toReturnState(emptyState);
  });

  it('should handle REMOVE_PRODUCT action by id', () => {
    Reducer(cartReducer)
      .withState(initialStateWithProduct)
      .expect(removeProductAction)
      .toReturnState(emptyState);
  });

  it('should handle REMOVE_PRODUCT action without id', () => {
    removeProductAction.payload = {};

    Reducer(cartReducer)
      .withState(initialStateWithProduct)
      .expect(removeProductAction)
      .toReturnState(initialStateWithProduct);
  });

  it('should handle UPDATE_QUANTITY action', () => {
    const nextQuantityValue = 3;

    updateQuantityAction.payload.quantity = nextQuantityValue;

    const stateWithUpdatedQuantity = merge({}, initialStateWithProduct, {
      quantityByIds: {
        [id]: nextQuantityValue,
      },
    });

    Reducer(cartReducer)
      .withState(initialStateWithProduct)
      .expect(updateQuantityAction)
      .toReturnState(stateWithUpdatedQuantity);
  });

  it('should handle UPDATE_QUANTITY action with empty payload', () => {
    updateQuantityAction.payload = {};

    Reducer(cartReducer)
      .withState(initialStateWithProduct)
      .expect(updateQuantityAction)
      .toReturnState(initialStateWithProduct);
  });
});
