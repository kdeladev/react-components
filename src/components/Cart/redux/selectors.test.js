import { Selector } from 'redux-testkit';
import nth from 'lodash/nth';

import cartReducer from './reducers';
import mockProducts from '../__mocks__/products';

import { addProduct } from './actions';
import { getProducts } from './selectors';

describe('Cart selectors', () => {
  let emptyState;
  let product1;
  let product2;

  beforeEach(() => {
    product1 = nth(mockProducts, 0);
    product2 = nth(mockProducts, 1);

    emptyState = {
      productsByIds: {},
      quantityByIds: {},
    };
  });

  it('should return empty array if state is empty', () => {
    Selector(getProducts)
      .expect(emptyState)
      .toReturn([]);
  });

  it('should return array with one product', () => {
    const stateWithProduct = cartReducer(undefined, addProduct(product1, 3));

    const product1WithQuantity = Object.assign({}, product1, {
      quantity: 3,
    });

    Selector(getProducts)
      .expect(stateWithProduct)
      .toReturn([product1WithQuantity]);
  });

  it('should return array with multiple products', () => {
    let stateWithProduct;

    stateWithProduct = cartReducer(undefined, addProduct(product1));
    stateWithProduct = cartReducer(stateWithProduct, addProduct(product2, 3));

    const product1WithQuantity = Object.assign({}, product1, {
      quantity: 1,
    });

    const product2WithQuantity = Object.assign({}, product2, {
      quantity: 3,
    });

    Selector(getProducts)
      .expect(stateWithProduct)
      .toReturn([product1WithQuantity, product2WithQuantity]);
  });
});
