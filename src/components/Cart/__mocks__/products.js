const products = [
  {
    id: '536125b5-f006-4cf4-9c75-adeb583b923c',
    name: 'Jersey Top',
    description: 'Bluza z grubej dzianiny dresowej. Kaptur z dżersejową podszewką i kopertowym przodem. Kangurkowa kieszeń, reglanowe rękawy i dół ze ściągaczem. Wewnątrz miękki meszek. Luźny fason.',
    image: 'http://lp2.hm.com/hmgoepprod?set=source[/3c/fb/3cfb34af3df371c1409247cbc8c93607d5276a3e.jpg],origin[dam],category[men_cardigansjumpers_jumpers],type[LOOKBOOK],hmver[1]&call=url[file:/product/main]',
    price: 70,
    categoryId: '0710171b-3820-4686-b5f0-9c29c7c74b1c',
    reviews: [
      {
        id: 'ab10f320-752b-4c23-a791-83649202c40e',
        text: 'TIL that when teens are taught that people have the potential to change their socially relevant traits over time, they tend to cope better with social stress, and end up earning better grades in school.',
        author: 'David Fincher',
      },
    ],
  },
  {
    id: '0608afc7-82aa-4d29-9f6a-db0705e1457f',
    name: 'Top',
    description: 'Bluza z dzianiny dresowej. Kaptur z podszewką i sznurkiem, suwak z przodu, kieszenie po bokach. Rękawy i dół ze ściągaczem. Fason normalny.',
    image: 'http://lp2.hm.com/hmgoepprod?set=source[/f9/0d/f90d051ff92e4125dbfaac9a815eb54601af3df4.jpg],origin[dam],category[men_jeans_skinny],type[LOOKBOOK],hmver[1]&call=url[file:/product/main]',
    price: 199,
    categoryId: '0710171b-3820-4686-b5f0-9c29c7c74b1c',
    reviews: [
      {
        id: '41ebeb8e-62c1-4809-a86d-5a54165662e9',
        text: 'The plaintiff in the famous “hot coffee case” offered to settle the case for $20,000 before trial, which McDonald’s refused',
        author: 'Tom Ford',
      },
      {
        id: 'ab10f320-752b-4c23-a791-83649202c40e',
        text: 'TIL that when teens are taught that people have the potential to change their socially relevant traits over time, they tend to cope better with social stress, and end up earning better grades in school.',
        author: 'David Fincher',
      },
    ],
  },
  {
    id: '2a4e1b04-90d6-450f-b23d-78f3cfd25612',
    name: 'Rib-knit Cardigan',
    description: 'Luźna bluza z dzianiny dresowej z nadrukiem motywu. Ściągacz wokół szyi, rękawów i u dołu.',
    image: 'http://lp2.hm.com/hmprod?set=source[/environment/2017/9GZ_0293_016R.jpg],width[3901],height[4561],y[-11],type[FASHION_FRONT]&hmver=0&call=url[file:/product/main]',
    price: 380,
    categoryId: '0710171b-3820-4686-b5f0-9c29c7c74b1c',
    reviews: [
      {
        id: 'ab10f320-752b-4c23-a791-83649202c40e',
        text: 'TIL that when teens are taught that people have the potential to change their socially relevant traits over time, they tend to cope better with social stress, and end up earning better grades in school.',
        author: 'David Fincher',
      },
      {
        id: '2a8976d9-cca7-424d-96ee-a4341dfb196a',
        text: 'TIL of René Carmille, a punched-card computer expert and French double agent who is believed to have saved thousands of lives by sabotaging Nazi efforts to identify Jewish citizens. He eventually was found out, withstood torture, and sent to a concentration camp where he died in January of 1945.',
        author: 'David Fincher',
      },
    ],
  },
];

export default products;
