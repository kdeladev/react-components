export const ADD_PRODUCT = '@@cart/ADD_PRODUCT';
export const REMOVE_PRODUCT = '@@cart/REMOVE_PRODUCT';
export const UPDATE_QUANTITY = '@@cart/UPDATE_QUANTITY';
